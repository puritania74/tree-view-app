import { Component, ViewChild } from '@angular/core';
import { ITreeNode, TreeNodeDndData } from './modules/tree-view/api';
import { TreeViewComponent } from './modules/tree-view/components/tree-view/tree-view.component';

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
} )
export class AppComponent {
  title = 'app works!';

  items1: Array<ITreeNode> = [
    { id: 1, parentId: null, title: 'Общий осмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотр' },
    { id: 2, parentId: null, title: 'Осмотр' },
    { id: 21, parentId: 2, title: 'Печень' },
    { id: 22, parentId: 2, title: 'Селезенка' },
  ];

  items2: Array<ITreeNode> = [
    { id: 1, parentId: null, title: 'Общий осмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотросмотр' },
    { id: 4, parentId: 1, title: 'Общее состояние' },
    { id: 5, parentId: 4, title: 'Нервно психический статус' },
    { id: 2, parentId: null, title: 'Осмотр' },
    { id: 6, parentId: 2, title: 'Пищеварительная система' },
    { id: 21, parentId: 8, title: 'Печень' },
    { id: 22, parentId: 8, title: 'Селезенка' },
  ];

  @ViewChild('tree1') tree1:TreeViewComponent;

  onSelect( item: ITreeNode ) {
    console.log( 'selected', item );
  }

  onDrop( $event: TreeNodeDndData ) {
    console.log( 'app.onDrop', $event.drag );
    let i = this.items1.indexOf( $event.drag );

    if( i !== -1 ) {
      this.items1.splice( i, 1 );
      this.items1 = [ ...this.items1 ];
    }
  }

  onChangeFilter(filter:string) {
    this.tree1.filter = filter;
    if (filter.length > 0) {
      this.tree1.expand();
    }
  }

  onChangeSearch(search:string) {
    this.tree1.highlightText = search;
  }

  onHighlight(items: Array<ITreeNode>) {
    console.log('Найдено ', items.length);
    console.log('search: ', items);
    items.forEach(item => this.tree1.expandTo(item));
  }

  markAsNew(item) {
    console.log(item);
  }

  onChange(value) {
    console.log(value);
  }
}
