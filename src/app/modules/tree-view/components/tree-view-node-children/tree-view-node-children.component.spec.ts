import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewNodeChildrenComponent } from './tree-view-node-children.component';

describe('TreeViewNodeChildrenComponent', () => {
  let component: TreeViewNodeChildrenComponent;
  let fixture: ComponentFixture<TreeViewNodeChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewNodeChildrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewNodeChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
