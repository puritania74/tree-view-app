import { Component, Input } from '@angular/core';
import { TreeNodeModel } from '../../models/tree-node.model';
import { TreeViewTemplates } from '../../api';

@Component({
  selector: 'app-tree-view-node-children',
  templateUrl: './tree-view-node-children.component.html',
  styleUrls: ['./tree-view-node-children.component.css']
})
export class TreeViewNodeChildrenComponent {

  @Input() nodes:Array<TreeNodeModel>;

  @Input() level: number;

  @Input() templates: TreeViewTemplates;

  constructor() { }

}
