import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewNodeExpanderComponent } from './tree-view-node-expander.component';

describe('TreeViewNodeExpanderComponent', () => {
  let component: TreeViewNodeExpanderComponent;
  let fixture: ComponentFixture<TreeViewNodeExpanderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewNodeExpanderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewNodeExpanderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
