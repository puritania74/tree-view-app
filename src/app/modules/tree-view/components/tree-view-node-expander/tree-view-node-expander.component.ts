import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { TreeNodeModel } from '../../models/tree-node.model';

@Component({
  selector: 'app-tree-view-node-expander',
  templateUrl: './tree-view-node-expander.component.html',
  styleUrls: ['./tree-view-node-expander.component.css']
})
export class TreeViewNodeExpanderComponent {

  @Input() node: TreeNodeModel;

  @Input() template: TemplateRef<any>;

  constructor() { }

  onClick() {
    this.node.expanded = !this.node.expanded;
  }

}
