import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewNodeContentComponent } from './tree-view-node-content.component';

describe('TreeViewNodeContentComponent', () => {
  let component: TreeViewNodeContentComponent;
  let fixture: ComponentFixture<TreeViewNodeContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewNodeContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewNodeContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
