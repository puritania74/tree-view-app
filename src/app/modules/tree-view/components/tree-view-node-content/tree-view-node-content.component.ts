import { Component, Input, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { TreeNodeModel } from '../../models/tree-node.model';
import { TreeModel } from '../../models/tree.model';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-tree-view-node-content',
  templateUrl: './tree-view-node-content.component.html',
  styleUrls: ['./tree-view-node-content.component.css']
})
export class TreeViewNodeContentComponent implements OnInit, OnDestroy {

  @Input() node: TreeNodeModel;

  @Input() template: TemplateRef<any>;

  highlightText:string = '';

  protected destroyed$: Subject<boolean> = new Subject();

  constructor(treeModel: TreeModel) {
    treeModel.highlight$.takeUntil( this.destroyed$ ).subscribe(highlight => this.highlightText = highlight)
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.destroyed$.next( true );
    this.destroyed$.complete();
  }
}
