import { Component, ContentChild, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef } from '@angular/core';
import { ITreeNode, ITreeView, TreeNodeDndData, TreeViewDndData } from '../../api';
import { TreeModel } from '../../models/tree.model';
import { TreeNodeService } from '../../services/tree-node.service';
import { TreeNodeModel } from '../../models/tree-node.model';
import { TreeViewDndService } from '../../services/tree-view-dnd.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component( {
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: [ './tree-view.component.css' ],
  providers: [
    TreeModel,
    TreeNodeService
  ]
} )
export class TreeViewComponent implements ITreeView, OnInit, OnDestroy {

  @ContentChild( 'treeNodeExpanderTemplate' ) expanderTemplate: TemplateRef<any>;
  @ContentChild( 'treeNodeContentTemplate' ) contentTemplate: TemplateRef<any>;

  @Input() set checkbox(checkbox:boolean) {
    this.treeModel.checkbox = checkbox;
  };

  @Input()
  set items( items: ITreeNode[] ) {
    this.treeModel.setItems( items );
  }

  @Input()
  set filter(filter: string) {
    this.treeModel.setFilter(filter);
  }

  @Input()
  set highlightText(text:string) {
    this.treeModel.setHighlight(text);
  }

  @Output() select: EventEmitter<ITreeNode> = new EventEmitter<ITreeNode>();
  @Output() dropNode: EventEmitter<TreeNodeDndData> = new EventEmitter<TreeNodeDndData>();
  @Output() changeValue: EventEmitter<ITreeNode[]> = new EventEmitter<ITreeNode[]>();
  @Output() highlight: EventEmitter<ITreeNode[]> = new EventEmitter<ITreeNode[]>();


  protected destroyed$: Subject<boolean> = new Subject();

  constructor( public treeModel: TreeModel ) {
    this.treeModel.selectedNodeId$.takeUntil( this.destroyed$ )
      .subscribe( id => {
        let selectedNodeModel = this.treeModel.getNodeModelById( id );

        this.select.emit( selectedNodeModel ? selectedNodeModel.data : null );
      } );

    this.treeModel.drop$.takeUntil( this.destroyed$ )
      .subscribe( data => this.dropNode.emit( { drag: data.drag.data, drop: data.drop.data } ) );

    this.treeModel.value$.takeUntil(this.destroyed$)
      .subscribe((models:Array<TreeNodeModel>) => {
        this.changeValue.emit(models.map(model => model.data));
      });

    this.treeModel.highlightNodeIds$.takeUntil(this.destroyed$)
      .subscribe(ids => {
        let items = ids.map(id => this.treeModel.getNodeModelById(id))
          .filter(model => model !== null)
          .map(model => model.data);

        this.highlight.emit(items);
      });
  }

  expand(): void {
    this.treeModel.expand();
  }

  collapse(): void {
    this.treeModel.collapse();
  }

  expandTo( item: ITreeNode ) {
    this.treeModel.expandTo(item);
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.destroyed$.next( true );
    this.destroyed$.complete();
  }
}
