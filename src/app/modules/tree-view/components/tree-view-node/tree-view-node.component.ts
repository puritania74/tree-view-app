import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { TreeNodeModel } from '../../models/tree-node.model';
import { TreeViewTemplates } from '../../api';
import { TreeModel } from '../../models/tree.model';

@Component({
  selector: 'app-tree-view-node',
  templateUrl: './tree-view-node.component.html',
  styleUrls: ['./tree-view-node.component.css']
})
export class TreeViewNodeComponent implements OnInit {

  @Input() node: TreeNodeModel;

  @Input() level: number;

  @Input() templates: TreeViewTemplates;

  highlightDropZone: boolean = false;

  constructor(public treeModel: TreeModel) { }

  ngOnInit(): void {

  }

  onDragEnter(nodeModel:TreeNodeModel) {
    this.highlightDropZone = true;
  }

  onDragLeave(nodeModel:TreeNodeModel) {
    this.highlightDropZone = false;
  }

  onDrop(nodeModel: TreeNodeModel) {
    this.highlightDropZone = false;
    this.node.drop(nodeModel);
  }

  onClick() {
    this.node.selected = true;
  }


}
