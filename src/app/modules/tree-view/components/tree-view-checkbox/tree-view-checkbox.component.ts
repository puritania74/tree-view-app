import { Component, OnInit, Input } from '@angular/core';
import { TreeNodeModel } from '../../models/tree-node.model';

@Component({
  selector: 'app-tree-view-checkbox',
  templateUrl: './tree-view-checkbox.component.html',
  styleUrls: ['./tree-view-checkbox.component.css']
})
export class TreeViewCheckboxComponent implements OnInit {

  @Input() node: TreeNodeModel;

  get indeterminate():boolean {
    return this.node.checked === null
  };

  get checked():boolean {
    return this.node.checked !== false;
  }

  constructor() { }

  onClick() {
    this.node.checked = !this.node.checked;
  }

  ngOnInit() {
  }

}
