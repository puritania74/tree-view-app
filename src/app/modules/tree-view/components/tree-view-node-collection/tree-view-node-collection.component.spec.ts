import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewNodeCollectionComponent } from './tree-view-node-collection.component';

describe('TreeViewNodeCollectionComponent', () => {
  let component: TreeViewNodeCollectionComponent;
  let fixture: ComponentFixture<TreeViewNodeCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewNodeCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewNodeCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
