import { Component, Input } from '@angular/core';
import { TreeViewTemplates } from '../../api';
import { TreeNodeModel } from '../../models/tree-node.model';

@Component({
  selector: 'app-tree-view-node-collection',
  templateUrl: './tree-view-node-collection.component.html',
  styleUrls: ['./tree-view-node-collection.component.css']
})
export class TreeViewNodeCollectionComponent {

  @Input() templates: TreeViewTemplates;

  @Input() nodes: Array<TreeNodeModel>;

  @Input() level: number;

  constructor() { }

}
