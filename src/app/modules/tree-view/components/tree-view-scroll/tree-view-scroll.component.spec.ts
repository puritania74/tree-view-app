import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewScrollComponent } from './tree-view-scroll.component';

describe('TreeViewScrollComponent', () => {
  let component: TreeViewScrollComponent;
  let fixture: ComponentFixture<TreeViewScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
