import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tree-view-node-indent',
  templateUrl: './tree-view-node-indent.component.html',
  styleUrls: ['./tree-view-node-indent.component.css']
})
export class TreeViewNodeIndentComponent implements OnInit {

  @Input() level:number;

  levels:Array<number> = [];

  constructor() { }

  ngOnInit() {
    this.levels = new Array(+this.level);
  }

}
