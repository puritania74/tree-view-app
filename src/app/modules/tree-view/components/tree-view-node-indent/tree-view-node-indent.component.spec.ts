import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeViewNodeIndentComponent } from './tree-view-node-indent.component';

describe('TreeViewNodeIndentComponent', () => {
  let component: TreeViewNodeIndentComponent;
  let fixture: ComponentFixture<TreeViewNodeIndentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewNodeIndentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewNodeIndentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
