import { Injectable } from '@angular/core';
import { TreeNodeService } from '../services/tree-node.service';
import { TreeNodeModel } from './tree-node.model';
import { ExpandedNodeIds, ITreeNode, TreeViewDndData } from '../api';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/combineLatest';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

interface CheckedModelIds {
  [modelId:string]: boolean | null
}

@Injectable()
export class TreeModel {

  value$: Observable<TreeNodeModel[]>;
  roots$: Observable<TreeNodeModel[]>;
  highlight$: Observable<string>;
  highlightNodeIds$: Observable<string[]>;
  expandedNodeIds$: Observable<ExpandedNodeIds>;
  selectedNodeId$: Observable<string>;
  drop$: Observable<TreeViewDndData<TreeNodeModel>>;


  protected items: Array<ITreeNode> = [];
  protected roots: BehaviorSubject<TreeNodeModel[]> = new BehaviorSubject<TreeNodeModel[]>([]);
  protected filter: BehaviorSubject<string> = new BehaviorSubject<string>('');
  protected highlight: BehaviorSubject<string> = new BehaviorSubject<string>('');
  protected highlightNodeIds: BehaviorSubject<string[]> = new BehaviorSubject([]);
  protected expandedNodeIds: BehaviorSubject<ExpandedNodeIds> = new BehaviorSubject<ExpandedNodeIds>({});
  protected selectedNodeId: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  protected dropSubject: Subject<TreeViewDndData<TreeNodeModel>> = new Subject<TreeViewDndData<TreeNodeModel>>();
  protected checkedNodeIds: BehaviorSubject<CheckedModelIds> = new BehaviorSubject<CheckedModelIds>({});

  checkbox: boolean = false;

  constructor(protected treeNodeService: TreeNodeService) {
    this.roots$ = this.roots.asObservable();
    this.drop$ = this.dropSubject.asObservable();
    this.expandedNodeIds$ = this.expandedNodeIds.asObservable().distinctUntilChanged();
    this.selectedNodeId$ = this.selectedNodeId.asObservable().distinctUntilChanged();
    this.highlightNodeIds$ = this.highlightNodeIds.asObservable().distinctUntilChanged();
    this.highlight$ = this.highlight.asObservable().distinctUntilChanged();
    this.value$ = this.checkedNodeIds.asObservable().distinctUntilChanged().map(checkedNodeIds => {
      return Object.keys(checkedNodeIds)
        .filter(modelId => checkedNodeIds[modelId])
        .map(modelId => this.getNodeModelById(modelId))
        .filter(model => model !== null);
    });

    this.roots$.combineLatest(this.filter.asObservable(), this.highlight, (roots, filter, highlight ) => ({roots, filter, highlight}))
      .subscribe(({roots, filter, highlight}) => {
        console.log('combineLatest');
        console.log('combineLatest', roots, filter);
        console.log('combineLatest', roots, filter);
        this.applyFilter(roots, filter);
        this.applyHighlight(roots, highlight);
      });

  }

  protected applyHighlight(roots: Array<TreeNodeModel>, text: string) {

    let highlightModels:Array<TreeNodeModel> = [];

    let highlight = text.toUpperCase() || '';

    if (highlight.length > 0 ) {
      highlightModels = this.searchHighlightModels(roots, highlight)
    }

    this.highlightNodeIds.next(highlightModels.map(model => model.id));
  }

  protected searchHighlightModels(nodeModels: Array<TreeNodeModel>, highlight: string):Array<TreeNodeModel> {
    let models:Array<TreeNodeModel> = [];

    nodeModels.forEach(model => {
      if (this.matchFilter(model.title, highlight)) {
        models.push(model);
      }

      Array.prototype.push.apply(models, this.searchHighlightModels(model.children, highlight));
    });

    return models;
  }


  setHighlight(text: string) {
    this.highlight.next(text || '');
  }

  setFilter(filter: string) {
    this.filter.next(filter || '');
  }

  setItems(items: ITreeNode[]) {
    this.items = items;
    let roots = this.treeNodeService.createNodeModels(this, items);
    this.roots.next(roots);
  }

  protected applyFilter(roots:Array<TreeNodeModel>, filter: string) {
    this.applyNodeModelsFilter(roots, filter.toUpperCase());
  }

  protected applyNodeModelsFilter(nodeModels: Array<TreeNodeModel>, filter: string): boolean {
    let matched:boolean = false;
    nodeModels.forEach(nodeModel => {
      matched = this.applyNodeModelsFilter(nodeModel.children, filter) || matched;

      matched = matched || this.matchFilter(nodeModel.title, filter);
      nodeModel.hidden = !matched;
    });

    return matched;
  }

  protected matchFilter(text:string, filter: string) {
    return text.toUpperCase().indexOf(filter) !== -1;
  }

  setExpanded(treeNodeModel: TreeNodeModel, expanded: boolean) {
    let expandedNodeIds = Object.assign({}, this.expandedNodeIds.value);

    expandedNodeIds[treeNodeModel.id] = expanded;
    this.expandedNodeIds.next(expandedNodeIds);
  }

  isExpandedNode(treeNodeModel: TreeNodeModel) : boolean {
    return this.expandedNodeIds.value[treeNodeModel.id];
  }

  setSelectedNode(nodeId: string) {
    this.selectedNodeId.next(nodeId ? nodeId : null);
  }

  setChecked(treeNodeModel: TreeNodeModel, checked: boolean) {
    let checkedNodeIds:CheckedModelIds = Object.assign({}, this.checkedNodeIds.value);

    //@TODO update node and children
    let state:CheckedModelIds = this.checkNodeModel(treeNodeModel, checked);
    Object.keys(state).forEach(id => checkedNodeIds[id] = state[id]);

    //@TODO update parents
    for (let parent = treeNodeModel.parent; parent !== null; parent = parent.parent) {
      let parentCheck: boolean;
      let totalCount = parent.children.length;
      let checkedCount = parent.children
        .map(child => child.id in checkedNodeIds ? checkedNodeIds[child.id]: child.checked)
        .filter(checked => checked === true).length;

      if (checkedCount === totalCount) {
        parentCheck = true;
      } else if (checkedCount === 0) {
        parentCheck = false;
      } else {
        parentCheck = null;
      }
      checkedNodeIds[parent.id] = parentCheck;
    }

    this.checkedNodeIds.next(checkedNodeIds);
  }

  protected checkNodeModel(nodeModel: TreeNodeModel, checked: boolean): CheckedModelIds {

    let state:CheckedModelIds = {};
    let check: boolean = checked;

    if (nodeModel.children.length > 0) {
      nodeModel.children.forEach(child => {
        if (child.checked === checked || child.hidden) {
          state[child.id] = checked;  //don't change
        } else {
          let childrenState = this.checkNodeModel(child, checked);
          Object.keys(childrenState).forEach(id => state[id] = childrenState[id]);
        }
      });

      let checkedCount = nodeModel.children.filter(child => state[child.id] === true).length;
      let totalCount = nodeModel.children.length;

      if (checkedCount === totalCount) {
        check = true;
      } else if (checkedCount === 0) {
        check = false;
      } else {
        check = null;
      }
    }

    state[nodeModel.id] = check;
    return state;
  }

  getChecked(treeNodeModel: TreeNodeModel):boolean {
    let checked = this.checkedNodeIds.value[treeNodeModel.id];

    return checked === void 0 ? false : checked;
  }

  expand() {
    let expanded = {};
    this.items.forEach(item => expanded[item.id.toString()] = true);
    this.expandedNodeIds.next(expanded);
  }

  expandTo(item: ITreeNode) {
    let expanded = {};
    let parentId = item.parentId;

    while(parentId !== null && parentId !== void 0) {
      expanded[parentId] = true;
      let item = this.findItemById(parentId);
      parentId = item ? item.parentId : null;
    }

    this.expandedNodeIds.next(expanded);
  }

  collapse() {
    this.expandedNodeIds.next({});
  }

  isSelectedNode(nodeId: string):boolean {
    return nodeId ? this.selectedNodeId.value === nodeId : false;
  }

  getNodeModelById(id: string): TreeNodeModel {
    return this.treeNodeService.findNodeModelById(id, this.roots.getValue());
  }

  dragAndDrop(dragModel:TreeNodeModel, dropModel: TreeNodeModel) {
    this.dropSubject.next({drag: dragModel, drop: dropModel});
  }

  protected findItemById(itemId) {
    let i = this.items.findIndex(item => item.id === itemId);
    return i === -1 ? null : this.items[i];
  }

}
