import { TreeModel } from './tree.model';
import { ITreeNode } from '../api';

const PATH_SEPARATOR = '/';

export class TreeNodeModel {

  id: string;

  parentId: string;

  title: string;

  parent: TreeNodeModel = null;

  hidden: boolean = false;

  children: Array<TreeNodeModel> = [];

  constructor(protected treeModel: TreeModel,
              public data: ITreeNode) {
    this.id = data.id.toString();
    this.title = data.title;
    this.parentId = (data.parentId === null || data.parentId === void 0) ? null : data.parentId.toString();
  }

  get hasChildren():boolean {
    return this.children.length > 0;
  }

  get hasVisibleChildren():boolean {
    return this.children.filter(model => !model.hidden).length > 0;
  }

  get path(): string {
    let path: Array<string> = [this.title];
    for (let model = this.parent; model !== null; model = model.parent) {
      path.push(model.title);
    }
    path.reverse();

    return path.join(PATH_SEPARATOR);
  }

  get expanded():boolean {
    return this.treeModel.isExpandedNode(this);
  }

  set expanded(expanded: boolean) {
    this.treeModel.setExpanded(this, expanded);
  }

  get checked() : boolean {
    return this.treeModel.getChecked(this);
  }

  set checked(checked: boolean) {
    this.treeModel.setChecked(this, checked);
  }

  get selected() : boolean {
    return this.treeModel.isSelectedNode(this.id);
  }

  set selected(selected: boolean) {
    this.treeModel.setSelectedNode(this.id)
  }


  drop(treeNodeMode: TreeNodeModel) {
    this.treeModel.dragAndDrop(treeNodeMode, this);
  }


}
