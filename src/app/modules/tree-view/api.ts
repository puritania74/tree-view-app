import { TemplateRef } from '@angular/core';

export interface  ITreeNode {
  id: number | string;
  parentId: number | string;
  title: string;
}

export interface ExpandedNodeIds {
  [id: string]: boolean;
}

export interface TreeViewTemplates {
  expanderTemplate?: TemplateRef<any>;
  contentTemplate?: TemplateRef<any>;
}

export interface ITreeView {
  expand(): void;
  collapse(): void;
  expandTo(item: ITreeNode);

}
export interface TreeViewDndData<T> {
  drag: T;
  drop: T;
}

export interface TreeNodeDndData extends TreeViewDndData<ITreeNode> {

}


