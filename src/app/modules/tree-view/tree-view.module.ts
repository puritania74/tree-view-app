import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeViewComponent } from './components/tree-view/tree-view.component';
import { TreeViewNodeComponent } from './components/tree-view-node/tree-view-node.component';
import { TreeViewNodeChildrenComponent } from './components/tree-view-node-children/tree-view-node-children.component';
import { TreeViewNodeContentComponent } from './components/tree-view-node-content/tree-view-node-content.component';
import { TreeViewNodeExpanderComponent } from './components/tree-view-node-expander/tree-view-node-expander.component';
import { TreeViewNodeIndentComponent } from './components/tree-view-node-indent/tree-view-node-indent.component';
import { TreeViewScrollComponent } from './components/tree-view-scroll/tree-view-scroll.component';
import { TreeViewNodeCollectionComponent } from './components/tree-view-node-collection/tree-view-node-collection.component';
import { TreeViewDndService } from './services/tree-view-dnd.service';
import { DraggableDirective } from './directives/draggable.directive';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { TreeViewCheckboxComponent } from './components/tree-view-checkbox/tree-view-checkbox.component';
import { VisibleNodesPipe } from './pipes/visible-nodes.pipe';
import { HighlightPipe } from './pipes/highlight.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TreeViewComponent,
    TreeViewNodeComponent,
    TreeViewNodeChildrenComponent,
    TreeViewNodeCollectionComponent,
    TreeViewNodeContentComponent,
    TreeViewNodeExpanderComponent,
    TreeViewNodeIndentComponent,
    TreeViewScrollComponent,
    TreeViewCheckboxComponent,
    DraggableDirective,
    DropZoneDirective,
    VisibleNodesPipe,
    HighlightPipe
  ],
  exports: [TreeViewComponent],
  providers: [TreeViewDndService]
})
export class TreeViewModule { }
