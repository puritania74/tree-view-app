import { Pipe, PipeTransform } from '@angular/core';
import { TreeNodeModel } from '../models/tree-node.model';

@Pipe({
  name: 'visibleNodes',
  pure: false
})
export class VisibleNodesPipe implements PipeTransform {

  transform(nodeModels: Array<TreeNodeModel>): Array<TreeNodeModel> {
    return nodeModels.filter(model => !model.hidden);
  }

}
