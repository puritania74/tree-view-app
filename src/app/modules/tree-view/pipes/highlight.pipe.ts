import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlight',
  pure: false
})
export class HighlightPipe implements PipeTransform {

  className:string = 'highlight';

  transform(value: any, args?: any): any {
    let highlight:string = args;
    let result: string = value;

    if (highlight) {
      let replace = `<i class="${this.className}">${highlight}</i>`;

      highlight = highlight.toUpperCase();

      let start = 0;
      do {
        let i = result.toUpperCase().indexOf(highlight, start);

        if (i == -1) {
          break;
        }
        result = result.substr(0, i) + replace + result.substr(i + highlight.length);
        start = i + replace.length;
      } while(true);

    }

    return result;
  }

}
