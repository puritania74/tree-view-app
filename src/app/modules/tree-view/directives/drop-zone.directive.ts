import { Directive, EventEmitter, HostListener, Output } from '@angular/core';
import { TreeViewDndService } from '../services/tree-view-dnd.service';
import { TreeNodeModel } from '../models/tree-node.model';

@Directive({
  selector: '[dropZone]'
})
export class DropZoneDirective {

  @Output() dragEnter: EventEmitter<TreeNodeModel> = new EventEmitter<TreeNodeModel>();
  @Output() dragLeave: EventEmitter<TreeNodeModel> = new EventEmitter<TreeNodeModel>();
  @Output() dropNode: EventEmitter<TreeNodeModel> = new EventEmitter<TreeNodeModel>();
  @Output() dragOver: EventEmitter<TreeNodeModel> = new EventEmitter<TreeNodeModel>();

  constructor(protected treeViewDndService: TreeViewDndService) {
  }

  @HostListener('dragenter', ['$event']) onDragEnter($event) {
      this.dragEnter.emit(this.treeViewDndService.data);
      $event.preventDefault();
    }

  @HostListener('dragleave') onDragLeave() {
      this.dragLeave.emit(this.treeViewDndService.data);
    }

  @HostListener('dragover', ['$event']) onDragOver($event) {
    this.dragOver.emit(this.treeViewDndService.data);
      $event.preventDefault();
  }

  @HostListener('drop', ['$event']) onDrop($event) {
      this.dropNode.emit(this.treeViewDndService.data);
      $event.preventDefault();
  }

}
