import { Directive, HostListener, Input } from '@angular/core';
import { TreeNodeModel } from '../models/tree-node.model';
import { TreeViewDndService } from '../services/tree-view-dnd.service';

@Directive({
  selector: '[draggable]'
})
export class DraggableDirective {

  @Input() dragData: TreeNodeModel;


  constructor(protected treeViewDndService: TreeViewDndService ) {

  }

  @HostListener('dragstart', ['$event']) onDragStart($event) {
    this.treeViewDndService.dragStart(this.dragData);
    $event.dataTransfer.setData("text/plain", JSON.stringify(this.dragData.data));
  }

  @HostListener('dragend', ['$event']) onDragEnd($event) {
    this.treeViewDndService.dragEnd();
  }

}
