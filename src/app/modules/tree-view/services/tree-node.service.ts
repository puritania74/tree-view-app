import { Injectable } from '@angular/core';
import { TreeModel } from '../models/tree.model';
import { TreeNodeModel } from '../models/tree-node.model';
import { ITreeNode } from '../api';

const ROOT = '';

@Injectable()
export class TreeNodeService {

  constructor() {
  }

  createNodeModels( treeModel: TreeModel, items: Array<ITreeNode> ): Array<TreeNodeModel> {
    let models: Array<TreeNodeModel> = [];
    let byParent: { [parentId: string]: Array<TreeNodeModel> } = {};

    items.map( item => this.createNodeModel( treeModel, item ) ).forEach( model => {
      models.push( model );

      let parentId = model.parentId === null ? ROOT : model.parentId;
      if( !byParent[ parentId ] ) {
        byParent[ parentId ] = [];
      }

      byParent[ parentId ].push( model );
    } );

    //assign children
    models.forEach( model => {
      if( byParent[ model.id ] ) {
        model.children = byParent[ model.id ];
        model.children.forEach( child => child.parent = model );
      }
    } );

    return byParent[ ROOT ] ? byParent[ ROOT ] : [];
  }

  findNodeModelById( id: string, root: Array<TreeNodeModel> ): TreeNodeModel {
    return this.scanNodeModelsForId(id, root);
  }

  protected scanNodeModelsForId( id: string, models: Array<TreeNodeModel> ): TreeNodeModel {
    let found:TreeNodeModel = null;
    let index = models.findIndex( model => model.id === id );

    if( index !== -1){
      found = models[index];
    } else {
      for( let i = 0; i < models.length; i = i + 1 ) {
        found =  this.scanNodeModelsForId(id, models[i].children);
        if (found) {
          break;
        }
      }
    }
    return found;
  }

  protected createNodeModel( treeModel: TreeModel, data: ITreeNode ): TreeNodeModel {
    return new TreeNodeModel( treeModel, data );
  }

}
