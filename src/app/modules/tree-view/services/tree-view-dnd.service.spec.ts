import { TestBed, inject } from '@angular/core/testing';

import { TreeViewDndService } from './tree-view-dnd.service';

describe('TreeViewDndService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TreeViewDndService]
    });
  });

  it('should be created', inject([TreeViewDndService], (service: TreeViewDndService) => {
    expect(service).toBeTruthy();
  }));
});
