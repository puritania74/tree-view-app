import { Injectable } from '@angular/core';
import { ITreeNode } from '../api';
import { TreeNodeModel } from '../models/tree-node.model';

@Injectable()
export class TreeViewDndService {

  data: TreeNodeModel = null;

  constructor() { }


  dragStart(data: TreeNodeModel) {
    this.data = data;
  }

  dragEnd() {
    this.data = null;
  }

}
