import { TreeViewAppPage } from './app.po';

describe('tree-view-app App', () => {
  let page: TreeViewAppPage;

  beforeEach(() => {
    page = new TreeViewAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
